import Vue from "vue";
import Router from 'vue-router'

Vue.use(Router)

export default new Router({
  mode:'history',
  routes: [
    { path: "/", name: "home", component:()=>import('./views/Home.vue')},
    { path: "/contact", name: "contact", component: ()=>import('./views/Contact.vue') },
    { path: "/obraNueva", name: "obraNueva", component: ()=>import('./views/ObraNueva.vue') },
    { path: "/promos", name: "promos", component: ()=>import('./views/Promos.vue') },
    { path: "/ourprojects", name: "ourprojects", component: ()=>import('./views/OurProjects.vue') },
    { path: "/locales", name: "locales", component: ()=>import('./views/Locales.vue') },
    { path: "/reformas", name: "reformas", component: ()=>import('./views/Reformas.vue') },
    { path: "/diputacio", name: "diputacio", component: ()=>import('./views/Diputacio.vue') },
    { path: "/aboutUs", name: "aboutUs", component: ()=>import('./views/AboutUs.vue') },

  ]
})
